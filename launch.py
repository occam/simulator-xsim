#!/usr/bin/python2

import os
import json
from runner import run as get_command
from subprocess import call
from collections import OrderedDict


def read_json(path):
    contents = None
    with open(path) as f:
        contents = json.load(f)

    return contents


def write_json(path, contents):
    with open(path, "w") as f:
        f.write(
            json.dumps(contents, sort_keys=True, indent=4)
        )


manifest = read_json("../task.json")

helper_files = os.path.join("/", "usr", "share", "simulator-XSim")

executable = os.path.join(helper_files, "runner.py")

simulation = os.path.join(helper_files, "simulation.py")

try:
    input = manifest["inputs"][0]["connections"][0]["file"]
except (KeyError, IndexError):
    input = os.path.join(helper_files, "default_program.m")

try:
    configuration = manifest["inputs"][1]["connections"][0]["file"]
except (KeyError, IndexError):
    configuration = os.path.join(helper_files, "default_simulation.json")

output_file = "../outputs/0/0/statistics.json"

input_files = [input]
command = get_command(simulation,
                      configuration,
                      input_files,
                      output_file
                      )
print(command)
call(["/bin/bash", "-i", "-c", command])
