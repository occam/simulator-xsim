#include <mips_core_simulator/core.hpp>
#include <iostream>
#include <cmath>
#include <json/json.h>

//#define CORE_DEBUG_SIMPLE

#ifdef DEBUG
	#ifdef CORE_DEBUG_SIMPLE
		#define PDEBUG_SIMPLE(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
		#define PDEBUG(msg)
	#else
		#define PDEBUG_SIMPLE(msg) PDEBUG(msg)
		#ifdef CORE_DEBUG
			#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
		#else
			#define PDEBUG(msg)
		#endif
	#endif


	#ifdef PAUSE_DEBUG
		#define DPAUSE() std::cin.get()
	#else
		#define DPAUSE()
	#endif
#else
	#define PDEBUG_SIMPLE(msg)
	#define PDEBUG(msg)
	#define DPAUSE()
#endif

namespace implementation
{
namespace mips_core_simulator
{


XMIPSCore::XMIPSCore(std::string program, std::string latencies, std::string output, ProcessorState &state):
	instruction_set(latencies),
	state(state),
	output(output)
{
	program_loader(program);
}

bool XMIPSCore::add_execution(RawType_t *instruction_word, GenericInstruction *g_instruction)
{
	if(!execution_unit.unit)
	{
		execution_unit.unit = g_instruction;
		execution_unit.r_instruction = instruction_word;

		return true;
	}
	return false;
}

bool XMIPSCore::tick()
{

	if(execution_unit.unit)
	{
		if(execution_unit.unit->run(state))
		{
			// Increment count
			instruction_count[execution_unit.unit->get_name()]++;
			// Plot debug messages
			PDEBUG_SIMPLE("Executed: "<<std::endl<<std::endl<<"\t0x"<<std::hex<<execution_unit.r_instruction->instruction()<<std::dec<<": "<<*execution_unit.unit<<std::endl);
			PDEBUG_SIMPLE("State:"<<std::endl<<std::endl<<"\t"<<state<<std::endl);

			// Check if execution is done
			if(execution_unit.unit->get_name()=="halt")
			{
				terminate=true;
			}

			// Execution is done, cleanup
			delete execution_unit.unit;
			execution_unit.unit = nullptr;
			delete execution_unit.r_instruction;
			execution_unit.r_instruction = nullptr;

			return true;
		}
	}
	return false;
}



bool XMIPSCore::run_cycle()
{
	cycle_count++;
	// Just print we are running
	if(first_time)
	{
		std::cout<<"#####STARTING PROGRAM#####"<<std::endl;
		first_time=false;
	}

	PDEBUG("####################STARTING CYCLE####################");
	PDEBUG("cycle_count = "<<cycle_count);
	PDEBUG("is_data_memory_busy = "<<state.is_data_memory_busy());

	if(!state.is_data_memory_busy())
	{
		//Do we have an instruction?
		if(!r_instruction)
		{
			/** Fetch instruction **/
			uarch_t pc = state.get_pc();
			r_instruction = new RawType_t(state.get_instruction_memory().getw(pc));
			// Some debug info
			PDEBUG_SIMPLE("Fetched:"<<std::endl<<std::endl<<"\t"<<pc<<": 0x"<<std::setfill('0')<<std::setw(4)<<std::hex<<r_instruction->instruction()<<std::dec<<std::endl);
			PDEBUG_SIMPLE("State:"<<std::endl<<std::endl<<"\t"<<state<<std::endl);

		}

		// Can the instruction run?
		// TODO: improve
		if(!execution_unit.unit)
		{
			InstructionSet::GenericInstruction *g_instruction = instruction_set.get_instruction(*r_instruction);
			add_execution(new RawType_t(*r_instruction), g_instruction);
		}
		else
		{
			PDEBUG("Busy"<<std::endl);
			PDEBUG("State:"<<std::endl<<std::endl<<"\t"<<state<<std::endl);
		}
	}
	if(tick())
	{
		// Clean this up
		delete r_instruction;
		r_instruction=nullptr;
	}
	else
	{
		// Dont pause debug here
		PDEBUG("####################FINISHED CYCLE####################");
		PDEBUG(std::endl<<std::endl);
		return true;
	}
	PDEBUG("####################FINISHED CYCLE####################");
	PDEBUG(std::endl<<std::endl);
	DPAUSE();

	if(terminate)
	{
		std::cout<<"#####FINISHED PROGRAM#####"<<std::endl;
		write_log();
		return false;
	}
	return true;

}

void XMIPSCore::program_loader(std::string path)
{
	uarch_t temp_pc=0;
	std::ifstream f(path);
	if(!f.is_open())
	{
		PERROR("Invalid program file: "<<path);
		exit(EXIT_FAILURE);
	}
	for( std::string line; getline( f, line ); )
	{
		std::size_t first_comment=line.find("#");
		std::size_t first_hex=line.find("0x");
		std::size_t first_number = line.find_first_of("0123456789ABCDEFabcdef");

		if( (first_hex<first_comment) || (first_number<first_comment) )
		{
			if(first_hex<=first_number)
			{
				std::stringstream ss;
				ss<<std::hex<<line.substr(first_hex,sizeof(uarch_t)*2+2);
				uarch_t instruction;
				ss>> instruction;
				PDEBUG(ss.str());
				state.get_instruction_memory().setw(temp_pc, instruction);
				temp_pc++;
				PDEBUG("0x"<<std::setfill('0')<<std::setw(4)<<std::hex<<instruction<<std::dec);
			}
			else
			{
				std::stringstream ss;
				ss<<std::hex<<line.substr(first_number,sizeof(uarch_t)*2);
				uarch_t instruction;
				ss>> instruction;
				PDEBUG(ss.str());
				state.get_instruction_memory().setw(temp_pc, instruction);
				temp_pc++;
				PDEBUG("0x"<<std::setfill('0')<<std::setw(4)<<std::hex<<instruction<<std::dec);
			}
		}
	}
}

void XMIPSCore::write_log()
{
	std::ofstream f(output);
	Json::Value jsonRoot;
	Json::Value jsonRegisters;
	for(std::size_t i=0; i<state.get_registers().size();i++)
	{
		std::stringstream ss;
		ss<<"r"<<i;
		jsonRegisters[ss.str()]=state.get_register(i);
	}
	Json::Value jsonRegistersArray(Json::arrayValue);
	jsonRegistersArray.append(jsonRegisters);
	jsonRoot["registers"]=jsonRegistersArray;

	Json::Value jsonStats;
	uint64_t total_instructions=0;
	for(const std::pair<std::string, std::size_t> p:instruction_count)
	{
		jsonStats[p.first]=Json::Value::UInt64(p.second);
		total_instructions+=p.second;
	}
	jsonStats["instructions"]=Json::Value::UInt64(total_instructions);
	jsonStats["cycles"]=Json::Value::UInt64(cycle_count);

	Json::Value jsonStatsArray(Json::arrayValue);
	jsonStatsArray.append(jsonStats);
	jsonRoot["stats"]=jsonStatsArray;

	f << jsonRoot << std::endl;
	f.close();

}


}
}
