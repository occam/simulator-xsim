#include <mips_core_simulator/instruction_set/instructions/generic_instruction.hpp>

#ifdef DEBUG
	#ifdef IS_DEBUG
		#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
	#else
		#define PDEBUG(msg)
	#endif
#else
	#define PDEBUG(msg)
#endif

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

std::string GenericInstruction::get_type() const
{
	return type;
}


std::string GenericInstruction::get_name() const
{
	return name;
}

GenericInstruction::uarch_t GenericInstruction::get_latency() const
{
	return latency;
}

GenericInstruction::uarch_t GenericInstruction::get_opcode() const
{
	return opcode;
}

std::ostream &operator<<(std::ostream &out, const GenericInstruction &instruction)
{
	out<<instruction.get_name()<<": "<<instruction.get_opcode()<<" of type "<<instruction.get_type()<<" latency of "<<instruction.get_latency();
	return out;

}

}
}
}
