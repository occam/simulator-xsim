#include <mips_core_simulator/instruction_set/instructions/jalr.hpp>

#ifdef DEBUG
#ifdef IS_DEBUG
#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
#else
#define PDEBUG(msg)
#endif
#else
#define PDEBUG(msg)
#endif

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

Jalr::Jalr(const RawType_t raw_instruction, uarch_t latency)
{
	this->latency=(latency==0)?(1):(latency);
	opcode=19;
	type="R";
	name="jalr";
	this->raw_instruction=raw_instruction;
}

bool Jalr::run(ProcessorState &processor_state)
{
	RType_t instruction(raw_instruction);
	uarch_t &rd=processor_state.get_register(instruction.rd());
	uarch_t &rs=processor_state.get_register(instruction.rs());
	uarch_t &rt=processor_state.get_register(instruction.rt());
	uarch_t &pc=processor_state.get_pc();

	cycles_spent++;
	if(cycles_spent>=latency)
	{
		processor_state.increment_pc();
		rd = (pc)<<1;
		pc = rs>>1;

		PDEBUG(name<<" "<<instruction);
		return true;
	}
	return false;
}



}
}
}

