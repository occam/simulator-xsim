#include <mips_core_simulator/instruction_set/instructions/bp.hpp>

#ifdef DEBUG
#ifdef IS_DEBUG
#define PDEBUG(msg) std::cout<<"Debug("<<basename(__FILE__)<<":"<<__FUNCTION__<<","<<__LINE__<<")"<<": "<<msg<<std::endl
#else
#define PDEBUG(msg)
#endif
#else
#define PDEBUG(msg)
#endif

namespace implementation
{
namespace mips_core_simulator
{
namespace instructions
{

Bp::Bp(const RawType_t raw_instruction, uarch_t latency)
{
	this->latency=(latency==0)?(2):(latency);
	opcode=20;
	type="I";
	name="bp";
	this->raw_instruction=raw_instruction;
}

bool Bp::run(ProcessorState &processor_state)
{
	IType_t instruction(raw_instruction);
	uarch_t imm8=instruction.imm8();
	uarch_t &rd=processor_state.get_register(instruction.rd());
	uarch_t &pc = processor_state.get_pc();

	cycles_spent++;
	if(cycles_spent>=latency)
	{
		if((arch_t)rd>0)
		{
			pc=imm8;
		}
		else
		{
			processor_state.increment_pc();
		}

		PDEBUG(name<<" "<<instruction);
		return true;
	}
	return false;
}



}
}
}
