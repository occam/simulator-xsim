#include <mips_core_simulator/instruction_set/instruction_types/i_type_t.hpp>
#include <sstream>

namespace implementation
{
namespace mips_core_simulator
{
namespace instruction_types
{
IType_t::IType_t(const RawType_t &other):
	RawType_t(other)
{
}

IType_t::IType_t(RawType_t &&other):
	RawType_t(other)
{
}

IType_t::uarch_t IType_t::rd() const
{
	return get_range<8,10>();
}

IType_t::uarch_t IType_t::imm8() const
{
	return get_range<0,7>();
}


std::string IType_t::to_string() const
{
	std::stringstream ss;
	ss<<"I op("<<opcode()<<"), r"<<rd()<<", imm8 = "<<imm8();
	return ss.str();
}


std::ostream& operator<<(std::ostream& out, const IType_t& inst)
{
	out<<inst.to_string();
	return out;
}

}
}
}
