#include <mips_core_simulator/instruction_set/instruction_types/raw_type_t.hpp>
#include <sstream>

namespace implementation
{
namespace mips_core_simulator
{
namespace instruction_types
{

RawType_t::RawType_t():
	RawType_t(0)
{
}

RawType_t::RawType_t(uarch_t instruction):
	Super(instruction)
{
}

RawType_t::uarch_t RawType_t::opcode() const
{
	return get_range<11,15>();
}

RawType_t::uarch_t RawType_t::instruction() const
{
	return get_range<0,15>();
}

std::string RawType_t::to_string() const
{
	std::stringstream ss;
	ss<<"Raw,"<<opcode()<<"::"<<std::hex<<instruction()<<std::dec;
	return ss.str();
}


std::ostream& operator<<(std::ostream& out, const RawType_t& inst)
{
	out<<inst.to_string();
	return out;
}

}
}
}
