#ifndef __MEMORY_HPP__
#define __MEMORY_HPP__

#include <config/arch_config.hpp>
#include <mips_core_simulator/memory_interface/memory_interface.hpp>
#include <array>
#include <cstdint>

namespace implementation
{

namespace memory
{
using namespace arch;
class Memory
{

	public:
		Memory()=default;

		Memory(const Memory &other) = default;
		Memory(Memory &&other) = default;

		Memory &operator=(const Memory &other) = default;
		Memory &operator=(Memory &&other) = default;

		virtual ~Memory() noexcept = default;

		uarch_t getw(uarch_t address) const;
		void setw(uarch_t address, uarch_t data);

	protected:
		uint8_t get(uarch_t address) const;
		void set(uarch_t address, uint8_t data);

		std::array<uint8_t,65536> memory;
	private:

};

}
}

#endif
