#pragma once
#include <cstdint>
#include <config/arch_config.hpp>
#include <mips_core_simulator/memory_interface/memory_interface.hpp>
#include <mips_core_simulator/memory/memory.hpp>
#include <zero_latency_memory.hpp>
//#include <simpleMIPS/sst_memory.hpp>


namespace implementation
{
namespace state
{


class ProcessorState
{
	public:
		using register_t = std::array<arch::uarch_t, 8>;
		using Memory = implementation::memory::Memory;
		using uarch_t = arch::uarch_t;
	public:

		ProcessorState()=delete;
		ProcessorState(MemoryLatencyI &latency_simulator);

		virtual ~ProcessorState() noexcept = default;
		void increment_pc();
		void set_pc(const arch::uarch_t &get_pc);
		void get_pc(arch::uarch_t &get_pc);
		const arch::uarch_t &get_pc() const;
		arch::uarch_t &get_pc();

		const register_t &get_registers() const;
		register_t &get_registers();
		const uarch_t &get_register(uarch_t n) const;
		uarch_t &get_register(uarch_t n);

		const Memory &get_instruction_memory() const;
		Memory &get_instruction_memory();
		const Memory &get_data_memory() const;
		Memory &get_data_memory();

		bool is_data_memory_busy() const;
		void simulate_data_memory_read(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb);
		void simulate_data_memory_write(uarch_t id, uarch_t address, std::function<void(uarch_t, uarch_t)> cb);

	protected:
		uarch_t pc=0;

		register_t registers={{0}};

		// Storage (no latency access)
		Memory data_memory;
		Memory instruction_memory;
		// Latency simulator only 1 permited
		MemoryLatencyI &data_memory_latency;
		bool data_memory_access_in_progess=false;

		std::string to_string() const;
		friend std::ostream& operator<<(std::ostream& out, const ProcessorState& state);

};
std::ostream& operator<<(std::ostream& out, const ProcessorState& state);


}
}
