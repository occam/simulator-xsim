#ifndef __IX_TYPE_T_HPP__
#define __IX_TYPE_T_HPP__

#include <mips_core_simulator/instruction_set/instruction_types/raw_type_t.hpp>

namespace implementation
{
namespace mips_core_simulator
{

namespace instruction_types
{
class IXType_t:
		public RawType_t
{

	public:
		IXType_t() = default;

		IXType_t(const IXType_t &other) = default;
		IXType_t(IXType_t &&other) = default;

		IXType_t(const RawType_t &other);
		IXType_t(RawType_t &&other);

		IXType_t &operator=(const IXType_t &other) = default;
		IXType_t &operator=(IXType_t &&other) = default;

		virtual ~IXType_t() noexcept = default;

		uarch_t imm11() const;

	protected:
		virtual std::string to_string() const override;

	private:
		friend std::ostream& operator<<(std::ostream& out, const IXType_t& inst);

};
std::ostream& operator<<(std::ostream& out, const IXType_t& inst);

}
}
}

#endif
