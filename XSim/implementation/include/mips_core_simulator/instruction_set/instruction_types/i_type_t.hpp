#ifndef __I_TYPE_T_HPP__
#define __I_TYPE_T_HPP__

#include <mips_core_simulator/instruction_set/instruction_types/raw_type_t.hpp>

namespace implementation
{
namespace mips_core_simulator
{

namespace instruction_types
{

class IType_t:
		public RawType_t
{

	public:
		IType_t() = default;

		IType_t(const IType_t &other) = default;
		IType_t(IType_t &&other) = default;

		IType_t(const RawType_t &other);
		IType_t(RawType_t &&other);

		IType_t &operator=(const IType_t &other) = default;
		IType_t &operator=(IType_t &&other) = default;

		virtual ~IType_t() noexcept = default;

		uarch_t rd() const;
		uarch_t imm8() const;

	protected:
		virtual std::string to_string() const override;

	private:
		friend std::ostream& operator<<(std::ostream& out, const IType_t& inst);

};
std::ostream& operator<<(std::ostream& out, const IType_t& inst);


}
}
}
#endif
