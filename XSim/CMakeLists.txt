project(XSim)
cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_FLAGS "-g")
set(CMAKE_CXX_FLAGS	"${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_CXX_FLAGS	"${CMAKE_CXX_FLAGS} -w")


## Custom CMake Modules will be in this directory
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

# Search for SST
find_package(SST 6.1.0)
if(NOT "${SST_FOUND}" EQUAL 1)
    find_package(SST 7.1.0 REQUIRED)
endif()

find_package(Jsoncpp REQUIRED)

include_directories(sst/include)
include_directories(standalone/include)
include_directories(implementation/include)
include_directories(${JSONCPP_INCLUDE_DIR})
include_directories(${SST_INCLUDE_DIRS})

set(CMAKE_BUILD_TYPE Debug)

add_library(
	${PROJECT_NAME}_CORE
	SHARED

	implementation/src/mips_core_simulator/core.cpp
	implementation/include/mips_core_simulator/core.hpp

	implementation/include/config/arch_config.hpp

)




#########################################
#		The state of the processor		#
#########################################
add_library(
	${PROJECT_NAME}_state
	SHARED

	implementation/src/mips_core_simulator/state/state.cpp
	implementation/include/mips_core_simulator/state/state.hpp

)

#########################################
#			The instructions			#
#########################################
file(GLOB instructions_src RELATIVE "" "implementation/src/mips_core_simulator/instruction_set/instructions/*.c??")
file(GLOB instructions_headers RELATIVE "" "implementation/include/mips_core_simulator/instruction_set/instructions/*.h??")
add_library(
	${PROJECT_NAME}_instruction_set
	SHARED


	implementation/src/mips_core_simulator/instruction_set/instruction_types/raw_type_t.cpp
	implementation/include/mips_core_simulator/instruction_set/instruction_types/raw_type_t.hpp

	implementation/src/mips_core_simulator/instruction_set/instruction_types/r_type_t.cpp
	implementation/include/mips_core_simulator/instruction_set/instruction_types/r_type_t.hpp

	implementation/src/mips_core_simulator/instruction_set/instruction_types/i_type_t.cpp
	implementation/include/mips_core_simulator/instruction_set/instruction_types/i_type_t.hpp

	implementation/src/mips_core_simulator/instruction_set/instruction_types/ix_type_t.cpp
	implementation/include/mips_core_simulator/instruction_set/instruction_types/ix_type_t.hpp


	implementation/src/mips_core_simulator/instruction_set/instruction_set.cpp
	implementation/include/mips_core_simulator/instruction_set/instruction_set.hpp

	${instructions_headers}
	${instructions_src}

	)
target_compile_options(${PROJECT_NAME}_instruction_set PUBLIC "-Wno-unused-variable")

#########################################
#		Other stuff that is not code	#
#########################################
# Just to show these files within QtCreator
file(GLOB stuff RELATIVE "" "tests/*")
add_custom_target(${PROJECT_NAME}_config ALL
	ls
	SOURCES
	example_configuration/config.py
	example_configuration/config2.py
	config/simulation.json
	config/test0.m
	config/test1.m
	config/config.json
	${stuff}
)



#########################################
#		Standalone memory storage		#
#########################################
add_library(
	${PROJECT_NAME}_memory
	SHARED

	implementation/src/mips_core_simulator/memory/memory.cpp
	implementation/include/mips_core_simulator/memory/memory.hpp
	standalone/src/zero_latency_memory.cpp
	standalone/include/zero_latency_memory.hpp



)
#########################################
#		Standalone executable			#
#########################################
add_executable(
	xsim_standalone
	standalone/src/main.cpp
)
target_link_libraries(
	xsim_standalone
	${PROJECT_NAME}_CORE
	${PROJECT_NAME}_instruction_set
	${JSONCPP_LIBRARIES}
	${PROJECT_NAME}_memory
	${PROJECT_NAME}_state
	)






#########################################
#				SST library				#
#########################################
# Create our library
add_library(
	${PROJECT_NAME}
	SHARED

	sst/src/simpleMIPS/mips_core.cpp
	sst/include/simpleMIPS/mips_core.hpp

	sst/src/simpleMIPS/sst_memory.cpp
	sst/include/simpleMIPS/sst_memory.hpp

	sst/src/simpleMIPS/mips_core_sst_interface.cpp
)
target_compile_options(${PROJECT_NAME} PUBLIC ${SST_CFLAGS} ${SST_LDFLAGS})
target_include_directories(${PROJECT_NAME} PUBLIC ${SST_INCLUDE_DIRS})
target_compile_definitions(${PROJECT_NAME} PUBLIC ${SST_DEFINITIONS})
add_dependencies(${PROJECT_NAME}
	${PROJECT_NAME}_instruction_set
	${PROJECT_NAME}_memory
	${PROJECT_NAME}_state
	)
# Remove all the SST warnings about deprecated functions.
target_compile_options(${PROJECT_NAME} PUBLIC "-Wno-deprecated")
target_compile_options(xsim_standalone PUBLIC "-Wno-deprecated")
target_link_libraries(
	${PROJECT_NAME}
	${PROJECT_NAME}_CORE
	${PROJECT_NAME}_instruction_set
	${JSONCPP_LIBRARIES}
	${PROJECT_NAME}_memory
	${PROJECT_NAME}_state
	)

install(TARGETS ${PROJECT_NAME}
                ${PROJECT_NAME}_CORE
                ${PROJECT_NAME}_instruction_set
                ${PROJECT_NAME}_memory
                ${PROJECT_NAME}_state
        DESTINATION lib)

install(TARGETS xsim_standalone
        DESTINATION bin)
