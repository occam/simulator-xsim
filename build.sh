PACKAGE_DESTINATION=$PWD/../build

mkdir -p build
cd build
cmake ../XSim -DCMAKE_INSTALL_PREFIX=/usr
make
DESTDIR=$PACKAGE_DESTINATION make install
cd ..

mkdir -p $PACKAGE_DESTINATION/usr/share/simulator-XSim
cp -f default_program.m $PACKAGE_DESTINATION/usr/share/simulator-XSim/.
cp -f default_simulation.json $PACKAGE_DESTINATION/usr/share/simulator-XSim/.
cp -f generate_output.py $PACKAGE_DESTINATION/usr/share/simulator-XSim/.
cp -f launch.py $PACKAGE_DESTINATION/usr/share/simulator-XSim/.
cp -f runner.py $PACKAGE_DESTINATION/usr/share/simulator-XSim/.
cp -f utils.py $PACKAGE_DESTINATION/usr/share/simulator-XSim/.
cp -f simulation.py $PACKAGE_DESTINATION/usr/share/simulator-XSim/.
