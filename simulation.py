import sys 
import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) 
sys.path.append(dir_path) 

import sst
import utils

input_parser = utils.input_parser()

cpu = sst.Component("XSim","XSim.core")
cpu.addParams(input_parser.get_cpu_params())

memory = sst.Component("data_memory", "memHierarchy.MemController")
memory.addParams(input_parser.get_memory_params())

#Comment this to add cache
# Connect memory and cpu
cpu_memory_link = sst.Link("cpu_data_memory_link")
cpu_memory_link.connect(
    (cpu,   "data_memory_link", input_parser.get_link_latency() ),
    (memory,"direct_link",      input_parser.get_link_latency() ) )


# Uncomment this to add cache
#if(input_parser.has_cache()):
#    # L1 Cache
#    l1_cache = sst.Component("l1cache", "memHierarchy.Cache")
#    l1_cache.addParams(input_parser.get_l1_params())
#
#    #L2 Cache
#    l2_cache = sst.Component("l2cache", "memHierarchy.Cache")
#    l2_cache.addParams(input_parser.get_cache_params("l2cache"))
#
#    # Links to connect memory hierarchy and cpu
#    cpu_l1_link = sst.Link("cpu_l1_link")
#    l1_l2_link = sst.Link("l1_l2_link")
#    l2_memory_link = sst.Link("l2_data_memory_link")
#
#    # Connections
#    cpu_l1_link.connect(
#        (cpu,       "data_memory_link",  input_parser.get_link_latency() ),
#        (l1_cache,   "high_network_0",   input_parser.get_link_latency() ) )
#    l1_l2_link.connect(
#        (l1_cache,    "low_network_0",   input_parser.get_link_latency() ),
#        (l2_cache,    "high_network_0",  input_parser.get_link_latency() ) )
#
#    l2_memory_link.connect(
#         (l2_cache,    "low_network_0",  input_parser.get_link_latency() ),
#         (memory,      "direct_link",    input_parser.get_link_latency() ) )
#
#    # Enable SST Statistics Outputs for this simulation
#    sst.setStatisticLoadLevel(1)
#    sst.enableAllStatisticsForComponentName("l1cache",
#                                            {"type":"sst.AccumulatorStatistic"})
#
#
#    # Do not change the configurations below
#    #   They're being used to create the output file
#    sst.setStatisticOutput("sst.statOutputCSV")
#    sst.setStatisticOutputOptions( {
#        "filepath"  : "sst-cache-stats.csv",
#        "separator" : ", "
#        } )
#else:
#    # Connect only memory and cpu
#    cpu_memory_link = sst.Link("cpu_data_memory_link")
#    cpu_memory_link.connect(
#        (cpu,   "data_memory_link", input_parser.get_link_latency() ),
#        (memory,"direct_link",      input_parser.get_link_latency() ) )
