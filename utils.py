import argparse
import json

parser = argparse.ArgumentParser(description='Configuration options for this SST simulation.')

parser.add_argument('--input-program',
                    dest="input_program",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The program to run in the simulator')

parser.add_argument('--output-file',
                    dest="output_file",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The path to the output file')

parser.add_argument('--simulation-config',
                    dest="sim_config",
                    nargs=1,
                    required=True,
                    action='store',
                    help='The json file with the simulation parameters')


# Used by the configuration.py file
class Configuration:

    args = None

    def __init__(self):
        self.args = parser.parse_args()

    # Parses the simulation configurationa and input files
    #  for the cpu parameters
    def get_cpu_params(self):
        with open(self.args.sim_config[0],'r') as f:
        	simulation_options = json.load(f)
        cpu_config = simulation_options.get("cpu",{})
        cpu_config["input_file"] = self.args.input_program[0]
        cpu_config["configuration_file"] = ""
        cpu_config["output_file"] = self.args.output_file[0]
        return cpu_config

    def __config_simpleMem(self, config, mem_config):
        for k,v in config.get("simpleMem",{}).items():
            mem_config["backend.%s"%(k)]=v
        return mem_config

    def __config_simpleDRAM(self, config, mem_config):
        for k,v in config.get("simpleDRAM",{}).items():
            mem_config["backend.%s"%(k)]=v
        return mem_config

    def get_memory_params(self):
        with open(self.args.sim_config[0],'r') as f:
            simulation_options = json.load(f)
        config=simulation_options.get("memory")
        if config:
            mem_config = {}
            mem_config["backend"] = config.get("backend")
            mem_config["backend.mem_size"] = config.get("backend.mem_size")
            mem_config["clock"] = config.get("clock")
            if mem_config["backend"] == "memHierarchy.simpleMem":
                mem_config=self.__config_simpleMem(config, mem_config)
            elif mem_config["backend"] == "memHierarchy.simpleDRAM":
                mem_config=self.__config_simpleDRAM(config, mem_config)
        return mem_config

    def get_link_latency(self):
        return "300ps"
    # Uncomment this to add cache
    #def has_cache(self):
    #    with open(self.args.sim_config[0],'r') as f:
    #        simulation_options = json.load(f)
    #    l1_config = simulation_options.get("l1cache",{})
    #    if(l1_config == {}):
    #        return False
    #    return True
    #
    #def get_l1_params(self):
    #    with open(self.args.sim_config[0],'r') as f:
    #        simulation_options = json.load(f)
    #    l1_config = simulation_options.get("l1cache",{})
    #    if(l1_config != {}):
    #        l1_config["L1"]=1
    #    return l1_config
    #
    #def get_cache_params(self, cache_name):
    #    with open(self.args.sim_config[0],'r') as f:
    #        simulation_options = json.load(f)
    #    l1_config = simulation_options.get("l1cache",{})
    #    cache_config = simulation_options.get(cache_name,{})
    #    if(cache_config != {}):
    #        cache_config["L1"]=0
    #        cache_config["cache_line_size"]=l1_config["cache_line_size"]
    #    return cache_config

## This class generates the final output from the experiments run
class OutputGenerator:
    def __output_cpu(self, cpu_output, simulation_configuration):
        cpu_output["clock_frequency"] = simulation_configuration.get("cpu").get("clock_frequency")
        cpu_output = {
          "cpu":cpu_output
        }
        return cpu_output

    def __output_memory(self, simulation_configuration):
        memory_output = {
            "memory": {
                "clock": simulation_configuration.get("memory").get("clock")
            }
        }
        return memory_output

    # Uncomment this to add cache
    #def __output_cache(self, name, sst_output, simulation_configuration):
    #    cache_output = {name:{}}
    #    has_cache = False
    #    cache_misses = None
    #    # Search across the SST output according to the statistics in:
    #    #   sst-info memHierarchy.Cache
    #    for row in sst_output:
    #        # Look for l1cache output (l1cache is the name of the component in the simulation configuration)
    #        if row[0].strip() == name:
    #            # Look for CacheMisses statistics
    #            if row[1].strip() == "CacheMisses":
    #                cache_misses = int(row[8])
    #    if cache_misses != None:
    #        cache_output.get(name)["cache_misses"]= cache_misses
    #        has_cache = True
    #    # Search across the configuration file provided by OCCAM according to the configuration schema
    #    l1cache_configuration = simulation_configuration.get(name, None)
    #    if l1cache_configuration != None:
    #        # Copy all configuration options to the output
    #        for k,v in l1cache_configuration.items():
    #            cache_output.get(name)[k]=str(v)
    #        has_cache = True
    #    if not has_cache:
    #        cache_output = None
    #    return cache_output

    # This function is called by the generate_output.py file to get the output of a single experiment
    def get_output(self, cpu_output, sst_output, simulation_configuration):
        cpu = self.__output_cpu(cpu_output, simulation_configuration)
        memory = self.__output_memory(simulation_configuration)
        output = {}
        output.update(cpu)
        output.update(memory)
        # Uncomment this to add cache
        #l1cache = None
        #l2cache = None
        #if sst_output != None:
        #    l1cache = self.__output_cache("l1cache", sst_output, simulation_configuration)
        #    l2cache = self.__output_cache("l2cache", sst_output, simulation_configuration)
        #if l1cache != None:
        #    output.update(l1cache)
        #if l2cache != None:
        #    output.update(l2cache)
        return output


def input_parser():
    return Configuration()

def output_parser():
    return OutputGenerator()
